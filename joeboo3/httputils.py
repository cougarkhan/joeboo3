"""
Utils 

Copyright (c) 2015 by Anuta Networks, Inc.
All rights reserved.

"""

import logging
import sys
import requests
import simplejson
from base_url import baseURL

requests.packages.urllib3.disable_warnings()


import base64
import re

import argparse
sh = None

def anutasession(username, password, organization=''):
    """ 
    Method to establish session with NCX Server.
    Args:
        username: username used for login
        password: password used for login
        organization: organization/tenant the user belongs to
    Returns: 
         Session Handler otherwise raises exception 
    """
    #username = bytes(username, 'UTF-8')
    #password = bytes(password, 'UTF-8')
    #organization = bytes(organization, 'UTF-8')


    global sh
#
    try:
        url = 'https://' + baseURL + ":443"

        #Instantiating Session
        sh = requests.Session()
        
        #Encoding username,password for Authorization Token
        if organization != '':
            authString = bytes((username + "|" + organization + ":" + password), "UTF-8")
            authKey2 = base64.b64encode(authString)

        else :
            authString = bytes((username + ":" + password), "UTF-8")
            authKey2 = base64.b64encode(authString)
            
        
        # Updating Headers
        sh.headers.update({"Content-Type":"application/json", "APIVersion":"1.0", \
                           "Content-Encoding":"identity", 'Accept-Encoding': None, "Authorization":"Basic " + authKey2.decode("UTF-8")})
        
        # Sending Login Request to NCX Server
        sh.post(url + '/login', verify = False)
        return sh
    except StandardError as e:
        sys.stdout.write("Error: Could not connect to Appliance - " + str(baseURL) + str(e))
        sys.stdout.write("Error: Unable to Connect to Appliance- Check once VM :"+str(baseURL)+ \
                         " is Up or Not,ErrorMessage is: "+str(e))
        sh = None
        sys.exit()



def convert(input):
    """
    Method To Convert Following:
    UniCode String     -->String
    UniCoded Dictionary -->Dictionary
    UniCoded List       -->List
    """
    
    #Converting each dict unicoded key and its associated unicoded value to dict of key , value
    if isinstance(input, dict):
        new_dict = {}
        for k, v in input.iteritems():
            new_dict[convert(k)] = convert(v)
        return new_dict

    #Converting each unicoded element in List to normal element 
    elif isinstance(input, list):
        new_list = []
        for element in input:
            new_list.append(convert(element))
        return new_list

    #Converting unicoded sting to normal string by encoding using utf-8  
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input


def build_responsedict(response, bLoadBody):
    """
    Method To Build Python Object from Json Object
    """
    #Building python object that contains status, reason, body
    responseDict = {}
    responseDict['status'] = response.status_code
    responseDict['reason'] = response.reason

    responseDict['body'] = None
    if bLoadBody == 'true':
        try:

            if type(response.text) == dict:
                responseDict['body'] = response.text
            else:
                responseDict['body'] = response.json()
        except StandardError as e:
            responseDict['body'] = None
            sys.stdout.write("Exception Raised " + str(e))

    # Converting unicoded responseDict to responseDict
    responseDict = convert(responseDict)
    return responseDict


def httppost(url, data_set, sh):
    """ HTTP Post Request 
    Args: 
        url : Rest API of the Request    
        data_set : Python Object that is sent as Payload to NCX Server.
    
    Returns: Python dictionary as response
    """
    
    
    res = ''

    #Appending VM-Ip to API
    url1 = 'https://' + baseURL + ":443" + url
    
    #Serialization- Encoding python object to json object
    request_json = simplejson.dumps(data_set)
    
    #Sending the request to NCX Server using API and Payload 
    try:
        res = sh.post(url1, request_json, verify = False)
    except StandardError as e:
        sys.stdout.write("Exception Raised " + str(e))

    #Deserialization- Decoding json object to python object with the help of build_responsedict.
    responsedict = build_responsedict(res, 'true')
    if  responsedict['body'] != None and type(responsedict['body']) is not bool and 'data' in responsedict['body']:
        responsedict['body'] = responsedict['body']['data']
    return responsedict
