# Import Modules
import os
import sys
from ldap3 import Server, Connection, operation, SIMPLE, SYNC, ASYNC, SUBTREE, ALL


class eldap_host:

    def __init__(self, uri, port, base_dn, username, password):
        self.name = "Cylon Basestar"
        self.uri = uri
        self.port = port
        self.base_dn = base_dn
        self.username = username
        self.password = password
        self.scope = SUBTREE
        self.filter = None
        self.add_attr = None
        self.add_dn = None
        self.add_result