# Import Modules
import os
import sys
import requests as r
import httputils
import cx_Oracle as cx
import mysql.connector
from ldap3 import Server, Connection, operation, SIMPLE, SYNC, ASYNC, SUBTREE, ALL
import logging
import logging.handlers
import time
import classes
import traceback

# Logging
log_creation_time = time.strftime('%Y%m%d%H%M%S')
log_file = "C:\\temp\\\pylogs\python_log_"
log_file_level = logging.DEBUG
#log_file_handler = logging.FileHandler("C:\\temp\\python.log", "w")
#log_file_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=5242880, backupCount=10)
#log_file_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=92160, backupCount=10)
log_file_handler = logging.handlers.TimedRotatingFileHandler(log_file, "d", 1, 10)
log_file_formater = logging.Formatter("%(asctime)s,%(levelname)s,%(module)s,%(message)s", "%Y%m%d-%H%M%S")
log_file_handler.setFormatter(log_file_formater)
log_file_handler.setLevel(log_file_level)

log_stream_level = logging.DEBUG
log_stream_handler = logging.StreamHandler()
log_stream_formater = logging.Formatter("%(message)s")
log_stream_handler.setFormatter(log_stream_formater)
log_stream_handler.setLevel(log_stream_level)

logging.root.addHandler(log_file_handler)
logging.root.addHandler(log_stream_handler)
logging.root.setLevel(log_file_level)

#logging.basicConfig(filename=log_file, filemode='w', format='%(asctime)s,%(levelname)s,%(module)s,%(message)s', datefmt='%Y%m%d-%H%M%S', level=log_level)

logger = logging.getLogger(__name__)

# Variables
network_db_uri = "sirius.dbs.it.ubc.ca"
network_db_port = 1521
network_db_sid = "devl11b"
network_db_dsn = cx.makedsn(network_db_uri,network_db_port,network_db_sid)
network_db_conn = None
network_db_conn_username = "mpal"
network_db_conn_password = "M1ch43lP"
network_db_query = "SELECT ROLE_NAME,VLAN_ID,VLAN_NAME,ROLE_VLAN_ID FROM NC_ROLE_VLAN_TBL"
network_db_rowcount = None

mysql_db_uri = "localhost"
mysql_db_port = 3306
mysql_db_dsn = "test"
mysql_db_conn = None
mysql_db_conn_username = "mpal"
mysql_db_conn_password = "S1mpl322!"
mysql_db_table = "UBC_CWLROLE"
mysql_db_add_query = None
mysql_db_delete_query = "DELETE FROM " + mysql_db_dsn + "." + mysql_db_table

eldap_host_uri = "ldaps://eldapprov.stg.id.ubc.ca"
#eldap_host_uri = "ldaps://eldapcons.stg.id.ubc.ca"
eldap_host_port = 636
eldap_server = None
eldap_server_conn = None
eldap_base_dn = "ou=VPN,ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
eldap_bind_account_username = "uid=nmc-svcanuta1,ou=Service Accounts,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca"
eldap_bind_account_password = "tj4yeTNCGJ0dDt6X2Iah"
eldap_search_scope = SUBTREE
eldap_search_filter = "(objectClass=groupOfNames)"
#eldap_search_filter = "*"
eldap_add_attributes = {'objectClass': ['groupOfNames','nestedGroup','top'], 'cn': None, 'description': None}
eldap_add_dn = None
eldap_add_response = None
eldap_reader = None

exit_code = 0

#ncx_rest_session = httputils.anutasession('restapi','S1mpl322!')

try:
    """
    Connect to all data sources and generate lists from results
    """
    galactica_actual = classes.oracle_host(network_db_uri, network_db_port, network_db_sid, network_db_conn_username, network_db_conn_password, network_db_query)
    galactica_actual.open()
    galactica_actual.execute()

    cylon_basestar = classes.eldap_host(eldap_host_uri, eldap_host_port, eldap_base_dn, eldap_bind_account_username, eldap_bind_account_password)
    cylon_basestar.open()
    cylon_basestar.search()

    pegasus_actual = classes.mysql_host(mysql_db_uri, mysql_db_port, mysql_db_dsn, mysql_db_conn_username, mysql_db_conn_password)
    pegasus_actual.open()
    pegasus_actual.set_cursor()
    
    """
    Update groups in LDAP Server
    Groups will be added in ou=VPN,ou=ANUTA,ou=Role,ou=Groups,ou=NMC,ou=SERVICES,dc=stg,dc=id,dc=ubc,dc=ca if they do not exist.
    """
    cylon_basestar.add(galactica_actual.result)

    """
    Update roles in MySQL Database
    All old records will be deleted from UBC_CWLROLES and then new records will be inserted.
    """
    pegasus_actual.delete()
    pegasus_actual.add(galactica_actual.result)
    pegasus_actual.commit()
except Exception as err:
    logger.error(traceback.format_exc())
    logger.error("Exiting due to termintating errors...Peace Out!")
    logger.error("<Drops Mic>")
    exit_code = 1
finally:
    galactica_actual.close()
    cylon_basestar.close()
    pegasus_actual.close()
    SystemExit(exit_code)
