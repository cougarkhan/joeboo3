# Import Modules
import os
import sys
from ldap3 import Server, Connection, operation, SIMPLE, SYNC, ASYNC, SUBTREE, ALL
import cx_Oracle as cx
import mysql.connector
import requests as r
import httputils
import simplejson
import logging


logger = logging.getLogger(__name__)
logger.level = logging.DEBUG

class oracle_host(object):

    def __init__(self, uri="localhost", port="1521", sid="ORCL", username="sa", password="", query=""):
        self.name = "Galactica Actual"
        self.uri = uri
        self.port = port
        self.sid = sid
        self.dsn = cx.makedsn(uri,port,sid)
        self.query = query
        self.username = username
        self.password = password
        self.conn = cx.Connection
        self.cursor = cx.Cursor
        self.result = ()


    def open(self):
        try:
            logger.debug("Opening connection to {} as {}".format(self.dsn, self.username))
            self.conn = cx.connect(self.username, self.password, self.dsn)
            logger.debug("Successfully connected to {}".format(self.conn.tnsentry))
        except Exception as err:
            logger.error("Error connecting to {} as {}".format(self.dsn, self.username))
            raise


    def execute(self):
        try:
            if logger.level == logging.DEBUG:
                logger.debug("Creating cursor()")
            self.cursor = self.conn.cursor()
            if logger.level == 10:
                logger.debug("Executing query {}".format(self.query))
            self.cursor.execute(self.query)
            self.result = self.cursor.fetchall()

            logger.debug("Result has {} records".format(len(self.result)))
            if logger.level == 10 and len(self.result) > 0:
                for entry in enumerate(self.result):
                    logger.debug(entry)
            else:
                logger.debug("Result is empty for {}".format(self.query))
        except Exception as err:
            logger.error("Error fetching SQL data with query {}".format(self.query))
            raise


    def close(self):
        try:
            logger.debug("Closing connection to {} as {}".format(self.dsn, self.username))
            self.cursor.close()
            self.conn.close()
        except Exception as err:
            logger.error("Error closing connection to ()".format(self.dsn))
            raise


class eldap_host(object):

    def __init__(self, uri="localhost", port=389, base_dn="", username="", password=""):
        self.name = "Cylon Basestar"
        self.uri = uri
        self.port = port
        self.base_dn = base_dn
        self.username = username
        self.password = password
        self.search_scope = SUBTREE
        self.search_filter = "(objectClass=groupOfNames)"
        self.search_attr = ["cn"]
        self.search_limit = 1000
        self.search_response = None
        self.search_result = None
        self.add_attr = {'objectClass': ['groupOfNames','nestedGroup','top'], 'cn': None, 'description': None}
        self.add_dn = None
        self.add_response = None
        self.server = None
        self.conn = None


    def open(self):
        try:
            logger.debug("Binding to LDAP Server {}:{} as {}".format(self.uri, self.port, self.username))
            self.server = Server(self.uri, port=self.port, use_ssl=True, get_info=ALL)
            self.conn = Connection(self.server, auto_bind=True, client_strategy=SYNC, user=self.username,password=self.password, authentication=SIMPLE, check_names=True)
            logger.debug("Successfully connected")
        except Exception as err:
            log.error("Unexpected error binding to LDAP Server {}:{} as {}".format(self.uri, self.port, self.username))
            raise


    def search(self):
        try:
            logger.debug("Searching in {} with the following options: filter: {} | scope: {} | attr: {} | limit: {}".format(self.base_dn, self.search_filter, self.search_scope, self.search_attr, self.search_limit))
            self.conn.search(search_base=self.base_dn,
                             search_filter=self.search_filter,
                             search_scope=self.search_scope,
                             attributes=self.search_attr,
                             size_limit=self.search_limit)
            self.search_response = self.conn.response
            self.search_result = self.conn.result
            if logger.level == logging.DEBUG:
                for entry in self.search_response:
                    logger.debug(entry)
        except Exception as err:
            logger.error("Error Searching in {} with the following options: filter: {} | scope: {} | attr: {} | limit: {}".format(self.base_dn, self.search_filter, self.search_scope, self.search_attr, self.search_limit))
            raise

            
    def add(self, add_list):
        try:
            logger.debug("Comparing Oracle result with LDAP result.  Adding new entries to LDAP Server.")

            for row in add_list:
                match = 0
                for entry in self.search_response:
                    ldap_entry = entry['raw_attributes']['cn'][0].decode("UTF-8")
                    if row[0] == ldap_entry:
                        #logger.debug("{} | {}".format(row[0], ldap_entry))
                        match = 1
                        break
                if match == 0:
                    #logger.debug("Adding {} group to {}:{}/{}".format(row[0], self.uri, self.port, self.base_dn))
                    self.add_attr["cn"] = str(row[0])
                    self.add_attr["description"] = "ROLE_VLAN_ID=" + str(row[3])
                    self.add_dn = "cn=" + str(row[0]) + "," + self.base_dn
                    logger.debug("Adding {}".format(self.add_dn))
                    self.conn.add(self.add_dn, None, self.add_attr)
                    self.add_response = self.conn.response
                    if logger.level == logging.DEBUG and self.add_response is not None:
                        logger.debug(self.add_response)
        except Exception as err:
            logger.error("Error adding LDAP entries to {}:{}/{}".format(row[0], self.uri, self.port, self.base_dn))
            raise


    def close(self):
        try:
            logger.debug("Unbinding from LDAP Server {}:{} as {}".format(self.uri, self.port, self.username))
            self.conn.unbind()
        except Exception as err:
            logger.error("Error unbinding from LDAP Server {}:{} as {}".format(self.uri, self.port, self.username))
            raise


class mysql_host(object):

    def __init__(self, uri="localhost", port=3306, dsn="", username="root", password=""):
        self.uri = uri
        self.port = port
        self.dsn = dsn
        self.username = username
        self.password = password
        self.table = "UBC_CWLROLE"
        self.add_query = None
        self.delete_query = None
        self.conn = None
        self.cursor = None
        self.result = None

    def open(self):
        try:
            logger.debug("Opening connection to MySqlD {}:{}/{} as {}".format(self.uri, self.port, self.dsn, self.username))
            self.conn = mysql.connector.connect(user=self.username, 
                                                password=self.password,
                                                host=self.uri,
                                                database=self.dsn)
            if self.conn.is_connected():
                logger.debug("Successfully connected")
        except Exception as err:
            logger.error("Error connecting to MySqlD {}:{}/{}".format(self.uri, self.port, self.dsn))
            raise


    def set_cursor(self):
        try:
            logger.debug("Creating cursor.")
            self.cursor = self.conn.cursor()
        except Exception as err:
            logger.error("Error creating cursor.")
            raise


    def delete(self):
        try:
            self.cursor.reset()
            logger.debug("Deleting all records from {}/{}".format(self.dsn, self.table))
            self.delete_query = "DELETE FROM " + self.dsn + "." + self.table
            self.cursor.execute(self.delete_query)
            #self.result = self.cursor.fetchall()
            #if logger.level == logging.DEBUG and len(self.result) > 0:
            #    for result in enumerate(self.result):
            #        logger.debug(result)
            #    logger.debug("Number of rows affected by statement '{}'".format(len(result)))
        except Exception as err:
            logger.error("Error deleting all records from {}/{}".format(self.dsn, self.table))
            raise

    def add(self, cwl_roles_to_add):
        try:
            self.cursor.reset()
            logger.debug("Adding {} roles to {}".format(len(cwl_roles_to_add), self.table))
            for entry in cwl_roles_to_add:
                if str(entry[2]) == str("None"):
                    self.add_query = "INSERT INTO " + self.dsn + "." + self.table + " (CWL_ROLE, VLAN_ID, VLAN_NAME) VALUES ('" + str(entry[0]) + "', '" + str(entry[1]) + "', NULL)"
                else:
                    self.add_query = "INSERT INTO " + self.dsn + "." + self.table + " (CWL_ROLE, VLAN_ID, VLAN_NAME) VALUES ('" + str(entry[0]) + "', '" + str(entry[1]) + "', '" + str(entry[2]) + "')"
                logger.debug(self.add_query)
                self.cursor.execute(self.add_query)
            #self.cursor.reset()
            #self.cursor.execute("SELECT count(*) from "  + self.dsn + "." + self.table)
            #row_count = self.cursor.rowcount

            #if int(row_count) == len(cwl_roles_to_add):
            #    logger.debug("{} Records updated successfully".format(row_count))
            #else:
            #    logger.error("Not all records were updated.  {}.{} has {} records".format(self.dsn, self.table, row_count))
            #    raise RuntimeError("Error updating records.  {}.{} has {} records".format(self.dsn, self.table, row_count))
        except Exception as err:
            logger.error("Error adding all records from {}/{}".format(self.dsn, self.table))
            raise


    def commit(self):
        try:
            logger.debug("Commiting to MySql database")
            self.conn.commit()
        except Exception as err:
            logger.error("Error commiting to MySql database")
            raise


    def close(self):
        try:
            logger.debug("Closing connection to MySqlD {}:{}/{} as {}".format(self.uri, self.port, self.dsn, self.username))
            self.cursor.close()
            self.conn.close()
        except Exception as err:
            logger.error("Error closing connection to MySqlD {}:{}/{} as {}".format(self.uri, self.port, self.dsn, self.username))
            raise




