# Import Modules
import os
import sys
import logging
import time


class logd(object):

    def __init__(self, name = str()):
        self.name = __name__
        self.file = "C:\\temp\\python_log_" + time.strftime('%Y%m%d%H%M%S') +".log"
        self.level = logging.DEBUG
        logging.basicConfig(filename=self.file, filemode='w', format='%(created)f %(module)s %(message)s', datefmt='%Y%m%d%H%M%S', level=self.level)
        self.log = logging.getLogger(self.name)
        self.INFO = 1
        self.WARNING = 2
        self.ERROR = 3
        self.DEBUG = 4


    def log(self, msg = str(), severity = int()):
        try:
            if severity is not int:
                raise RuntimeError("Severity is of type :str.  It shoudl be of type :int")
        
            if severity == 1:
                self.log.info(msg)
            elif severity == 2:
                self.log.warn(msg)
            elif severity == 3:
                self.log.error(msg)
            elif severity == 4:
                self.log.debug(msg)
            else:
                raise RuntimeError("Error writing " + severity + ":" + msg + " to log")
        except Exception as err:
            raise err


    def close(self):
        try:
            for handler in self.log.handlers:
                handler.close()
        except Exception as err:
            raise RuntimeError("Error closing handler " + handler)