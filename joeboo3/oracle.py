# Import Modules
import os
import sys
import cx_Oracle as cx
import logging

logger = logging.getLogger(__name__)

class oracle_host(object):

    def __init__(self, uri, port, sid, username, password, query):
        self.name = "Galactica Actual"
        self.uri = uri
        self.port = port
        self.sid = sid
        self.dsn = cx.makedsn(uri,port,sid)
        self.query = query
        self.username = username
        self.password = password
        self.conn = None
        self.cursor = None
        self.result = None


    def connect(self):
        try:
            logger.info("Connecting to {} as {}".format(self.dsn, self.username))
            self.conn = cx.connect(self.username, self.password, self.dsn)
        except Exception as err:
            logger.error("Error connecting to {} as {}".format(self.dsn, self.username))
            logger.error(err)
            raise err

    def execute(self):
        try:
            if logger.level == logging.DEBUG:
                logger.info("Creating cursor()")
            self.cursor = self.conn.cursor()
            if logger.level == 10:
                logger.debug("Executing query {}".format(self.query))
            logger.debug(self.conn.version)
            self.cursor.execute(self.query)
            self.result = self.cursor.fetchall()
            if logger.level == 10 and len(self.result) > 0:
                for entry in enumerate(self.result):
                    logger.debug(entry)
            else:
                logger.debug("Result is empty for {}".format(self.query))
        except Exception as err:
            logger.error("Error fetching SQL data with query {}".format(self.query))
            logger.error(err)
            raise err

    def close(self):
        try:
            self.conn.close()
        except Exception as err:
            logger.error("Error closing connection to ()".format(self.dsn))
            logger.error(err)
            raise err





